<div class="post"><img src="/images/1921-09-22-frank-hewitt-rig.jpg">Uncle Frank Lane Hewitt<br> March 11<sup>th</sup>, 1903 - April 5<sup>th</sup>, 1989</div>

<div class="post"><img src="/images/1982-09-26-frank-hewitt-article.jpg">
<p><i>Written by Skellytown correspondent Mary Cousins for the Pampa News</i>

<p> Rig building - of one size or another - has been a way of life 
for Franklin Lane Hewitt for 65 years.<br> To Skellytown residents, "Uncle Frank" Hewitt is an ex-mayor, a retired welder who uses his skills 
to build precisely accurate models of oilfield derricks.<br> As many also know, Hewitt has built the real thing since the early days of wooden 
derricks.

<p>Born in Cairo, W. Va. in 1903, Hewitt moved to Ponca City, Okla. at the age of 12 to live with an uncle. The uncle, Jack Jones, began teaching 
the youngster the craft of rig building.<br> Two years later Jones was killed on an oil rig, and young Hewitt - then only 14 years old - took to 
rig building himself in the oilfields of Oklahoma.<br>  Rig building at that time was mostly carpentry : erecting the wooden derricks and other 
oilfield equipment. Five men worked six days to complete a wooden rig : to build a 122 foot rotary derrick was two days of arduous work for six men.

<p>Erecting a rig was, at times, very dangerous. In 1918, the teenaged rig-builder came to Texas, to Burkburnett, where he stayed for a year. 
After that, his path across Texas is a roll call of early-day oilfield activity : Coleman to Breckenridge to Cross Plains to Pioneer, Rising Star, 
Mexia, Corsicana to Texarkana.<br> By 1936, the wooden rig was considered obsolete : steel derricks were springing up in their place.  Hewitt 
returned to Borger.<br> Now in his thirties, he learned a new trade : he became a welder for Phillips Petroleum Co. For three years, he welded 
for Phillips.<br> Then for several years, Hewitt travelled the oilfields again, as a "barnstorming" welder, on different pipelines across the 
country.

<p> While working on a pipeline in Creede, Colorado, he learned that a welder was needed in Skellytown.  In September, 1950, he came to the city 
and set up a welding shop.<br> About a year later he was married to <b>Ivanette Chesher</b>, whom he had met in Patton's Drug Store.<br> In 1952, 
Hewitt began work with Skelly Oil Company (Getty), and continued as a welder for the company for 20 years until his retirement.

<p>In 1960, Skellytown elected its first mayor : Frank Hewitt. During Uncle Frank's two years in office, the city saw many changes in water storage, 
sewage, street lighting and paving.<br> In 1972 he retired, and a new phase began. He returned to the oilfield derricks he knew so well, but on a 
different scale, meticulously welding models of the steel towers that had been a major part of his working life.<br> "In constructing these ornamental 
derricks, I begin the same way I did on building a regular size derrick." he explains. "I figure out how tall I want the derrick to be, then proceed to 
cut each piece to the desired size."<br> He then welds the parts together into the completed miniature. The completed product is a sight to behold.

<p>Hewitt's derricks are exact to scale in every feature, precisely like the full-size derricks he once worked on. He may spend 40 - 50 hours constructing 
a small derrick, complete with ladder.  The ladder alone may take a full day to build.<br> Hewitt's handiwork is well-known in the area, and has been on 
display at area banks, and in the Carson County Historical Museum.<br> His welding skill and sense of proportion he has used in making other ornaments as 
well: fireplace matchboxes, gratings, windmills, flowerpots and roadrunners.<br> Hewitt's home on Skellytown's Main Street is easy to find: it's the one 
with the number 711 on a large model derrick.<br> It marks the residence of a man who has spent a lifetime using his hands building things of great importance, 
or great beauty.
</div>

