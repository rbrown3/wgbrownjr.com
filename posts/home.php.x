<div class="post"><img src="/images/2016-02-25-1-cemetery-looks-closed.jpg">The cemetery looks closed...</div>
<div class="post"><img src="/images/2016-02-25-2-going-in-anyway.jpg">Walking in to visit Lotte</div>
<div class="post"><img src="/images/2016-02-25-3-cleaning-up.jpg">Cleaning up branches on the way</div>
<div class="post"><img src="/images/2016-02-25-4-lunenburg-cemetery.jpg">I love you and miss you</div>
<div class="post"><img src="/images/2016-02-22-frank-hewitt.jpg">Uncle Frank Hewitt<br><a target="_blank" href="/frank/">CLICK HERE to Read About Frank Hewitt of Skellytown, Texas</a></div>
<div class="post"><img src="/images/2016-02-21-poppa-and-cows.jpg"><a href="http://hollishillsfarm.com" target="_blank">Hollis Hills Farm</a> near Fitchburg State College</div>
<div class="post"><img src="/images/2016-02-14-my-tree.jpg">Family Tree from <a href="http://ancestry.com">Ancestry.com</a></div>
<div class="post"><img src="/images/2016-02-13-cold.jpg">4&deg; Fahrenheit... Happy I'm not in Amarillo :)</div>
<div class="post"><img src="/images/2016-02-12-skaters.jpg">Skating on Lake Whalom</div>
<div class="post"><img src="/images/2016-02-11-tracks-in-snow.jpg">Who's been messing up on my perfect snow :)</div>
<div class="post"><img src="/images/2016-02-08-shovelling.jpg">More snow today and I'm keepin' ahead of it.</div>
<div class="post"><img src="/images/2016-02-08-bring-it-on.jpg">I don't need no snowblower, I've got this stick</div>
<div class="post"><img src="/images/2016-02-05-snow.jpg">Lots of snow... this is why I love living up here :)</div>
<div class="post"><img src="/images/2016-02-06-get-it-started.jpg">Let's see if she'll start... Yep, this is the best snowblower ever.</div>
<div class="post"><img src="/images/2016-02-06-gas-it-up.jpg">Got gas?</div>
<div class="post"><img src="/images/2016-02-06-snowblowing.jpg">I'm 87 and I still got it</div>
<div class="post"><img src="/images/2016-02-06-blowing-the-driveway.jpg">Time to rescue the truck</div>
<div class="post"><img src="/images/2016-02-06-blowing-the-back.jpg">Restore access to the oil tanks... I have over 700 gallons which should last until summer</div>
<div class="post"><img src="/images/2016-02-06-blowing-the-front.jpg">Clearing a path to the front door. Ready for visitors so come on by.</div>
<div class="post"><img src="/images/2016-02-06-all-done.jpg">All done. Ready for the next storm on Monday.</div>
